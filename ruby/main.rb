require 'json'
require 'socket'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)

	@car_color = 'unk'
  	@throttle = 0
  	@track = nil
  	@last_car_angle = 0
  	@last_piece_index = 0
  	@last_piece_pos = 0
  	@velocity = 0
  	@hasTurbo = false

    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      case msgType
        when 'carPositions'
          drive(tcp, msgData)
        when 'turboAvailable'
        	@hasTurbo = true
        else
          case msgType
            when 'join'
              puts 'Joined'
            when 'yourCar'
              our_car(msgData['name'], msgData['color'])
          	when 'gameInit'
          		tcp.puts ping_message()
          		setup_track(msgData['race']['track'])          	
            when 'gameStart'
              puts 'Race started'
            when 'crash'
              puts 'Someone crashed'
            when 'dnf'
              puts 'disqualified'
            when 'gameEnd'
              list_results(msgData['results'], msgData['bestLaps'])
              tcp.puts ping_message()
            when 'error'
              puts "ERROR: #{msgData}"
          end

          puts "Got #{msgType}"
          tcp.puts ping_message()
      end
    end
  end

  def our_car(name, color)
  	@car_color = color
  	puts "Our car is color #{color} with driver #{name}"
  end

  def drive(tcp, data)
  	our_car = get_our_car(data)
  	current_piece_index = our_car['piecePosition']['pieceIndex']
  	current_piece = get_piece(current_piece_index)
  	next_piece = get_piece(current_piece_index + 1)

  	throttle = get_throttle(our_car, current_piece, current_piece_index, @throttle)
  	use_turbo = should_use_turbo(our_car, current_piece, current_piece_index, throttle)	

	if has_cars_in_front(our_car, data)
		if @hasTurbo && (current_piece['angle'].to_f.abs + next_piece['angle'].to_f.abs) <= 22.5 
			tcp.puts turbo_message("Going crazyyyyy....")
			@hasTurbo = false

			puts "Burn the rubber"
		elsif next_piece.include?('switch') && next_piece['switch'] && @velocity < 7.5
			current_lane_index = our_car['piecePosition']['lane']['startLaneIndex'].to_i
			
			if (current_lane_index > 0)
				tcp.puts switch_message('Left')
			else
				tcp.puts switch_message('Right')
			end

			puts "Overtaking"
		elsif throttle < 0.7
			throttle += 0.2
		end
	end

	tcp.puts throttle_message(throttle)	
	@throttle = throttle
  end

  def has_cars_in_front(our_car, data)
  	current_piece_index = our_car['piecePosition']['pieceIndex']  	
  	cars_on_piece = data.select {|k| k['piecePosition']['pieceIndex'] == current_piece_index }

  	in_piece_pos = our_car['piecePosition']['inPieceDistance']
  	all_cars_in_front = cars_on_piece.select {|k| 
		distance = k['piecePosition']['inPieceDistance'] - in_piece_pos
		result = distance >= 0 && distance < 50
	}
	
  	color = our_car['id']['color']
	cars_in_front = all_cars_in_front.select {|k| k['id']['color'] != color}

  	cars_in_front.count > 0
  end

  def should_use_turbo(our_car, current_piece, current_piece_index, throttle)
  	if @hasTurbo == false
  		return false
  	end

	return current_piece['useTurbo']  	
  end

  def get_throttle(our_car, current_piece, current_piece_index, current_throttle)
  	next_piece = get_piece(current_piece_index + 1)
  	last_piece = get_piece(@last_piece_index)

  	# velocity
  	actualMaxVelocity = current_piece['maxVelocity']
  	if (current_piece_index == @last_piece_index)
  		in_piece_pos = our_car['piecePosition']['inPieceDistance']
  		@velocity = in_piece_pos - @last_piece_pos
  		@last_piece_pos = in_piece_pos

  		#percentage of current piece done
  		distance = our_car['piecePosition']['inPieceDistance']
  		length = get_piece_length(current_piece)

  		completed = 0

  		if distance > 0
  			completed = (length - distance) / length
  		end
  	
  		maxVelocity = current_piece['maxVelocity']
  		maxVelocityNext = next_piece['maxVelocity']

  		# interpolated value between this data point and the next
  		actualMaxVelocity = (maxVelocity * completed) + (maxVelocityNext * (1.0 - completed)) 	
  	else
  		@last_piece_pos = 0
  	end


	# current slip angle of our car
  	car_angle = our_car['angle'].to_f
  	piece_angle = current_piece['angle'].to_f

  	# start from current throttle
  	throttle = current_throttle

  	# accelerate if less than max velocity
  	if @velocity <= actualMaxVelocity
  		throttle += 0.2
  	else
  		throttle -= 0.1
  	end

  	if @last_piece_index != current_piece_index && @velocity < 9
		if car_angle.abs < 20 && car_angle.abs < @last_car_angle.abs
	  		current_piece['maxVelocity'] += 0.25
	  	elsif car_angle.abs > 25 && car_angle.abs > @last_car_angle.abs
	  		last_piece['maxVelocity'] -= 0.25
	  	end
  	end  

  	if car_angle.abs < 30 && next_piece['angle'].to_f.abs < 22.5 && @velocity < 7
		throttle += 0.1
  	elsif car_angle.abs > 5 && car_angle.abs > @last_car_angle.abs
		throttle -= 0.2
  	end

	# at least move
	if throttle <= 0.0
		throttle = 0.0
	end

	# obey throttle limit
	if throttle > 1.0
		throttle = 1.0
	end

	throttle = throttle.round(2)

  	# puts "P:#{current_piece_index} a: #{piece_angle}, ca: #{car_angle} t: #{throttle} v: #{@velocity} tv: #{actualMaxVelocity}"
  	@last_car_angle = car_angle
  	@last_piece_index = current_piece_index
  	return throttle
  end

  def get_piece_length(piece)
  	if piece['length'] != nil
  		return piece['length']
  	else
  		return piece['radius']
  	end
  end

  def get_piece(piecePosition)
  	max_index = @track['pieces'].count - 1
  	index = piecePosition

  	if (piecePosition > max_index)
  		index = 0
  	end

  	return @track['pieces'][index]
  end

  def setup_track(track)
  	@track = track

  	i = 0

  	while i < @track['pieces'].count
  		#extra = get_piece(i + 2)
  		piece = get_piece(i)
  		current = piece['angle'].to_f
  		nextp = get_piece(i + 1)['angle'].to_f
  		third = get_piece(i + 2)['angle'].to_f
  		fourth = get_piece(i + 3)['angle'].to_f
  		fifth = get_piece(i + 4)['angle'].to_f

  		total = current.abs + nextp.abs + third.abs + fourth.abs + fifth.abs

  		if total == 0
  			piece['useTurbo'] = true
  		else
  			piece['useTurbo'] = false
  		end

  		if current.abs == 0
  			if nextp.abs == 0
  				piece['maxVelocity'] = 10.0
  			elsif nextp.abs >= 22.0
  				piece['maxVelocity'] = 7

  				if third.abs >= 45
  					piece['maxVelocity'] = 5
  				end
  			else
  				piece['maxVelocity'] = 8.0
  			end  			
  		elsif current >= 45
  			if nextp >= 22
  				piece['maxVelocity'] = 6
  			elsif nextp.abs == 0.0
  				piece['maxVelocity'] = 8.0
  			else
  				piece['maxVelocity'] = 8.0
  			end
  		elsif current <= -45
  			if nextp <= -22
  				piece['maxVelocity'] = 6
  			elsif nextp == 0.0
  				piece['maxVelocity'] = 7.5
  			else
  				piece['maxVelocity'] = 8.0
  			end
  		else
  			piece['maxVelocity'] = 8.0
  		end

  		i += 1
  	end

  	#puts @track['pieces']
  end

  def get_our_car(data)
  	cars = data.select {|k| k['id']['color'] == @car_color }
  	cars[0]
  end

  def list_results(results, bestLaps)
  	puts results
  	puts bestLaps
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def switch_message(data)
  	make_msg("switchLane", data)
  end

  def turbo_message(data)
    make_msg("turbo", data)
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end
end

NoobBot.new(server_host, server_port, bot_name, bot_key)
